axios.defaults.headers.common['X-CSRF-TOKEN'] = document.querySelector('#token').getAttribute('content');
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

if($("#formulario_contacto").length){
    let contacto = new Vue({
        el: "#formulario_contacto",
        data: {
            enviado: false,
            errors: [],
            loading: false,
            datos: {
                nombres: '',
                email: '',
                empresa: '',
                telefono: '',
                mensaje: ''
            }
        },
        methods: {
            guardar: function() {
                this.errors = [];
                this.loading = true;
                this.enviado = false;
                axios.post('/contacto', this.datos)
                    .then(response => {
                        this.enviado = true;
                        this.loading = false;
                        this.datos = {
                            nombres: '',
                            email: '',
                            celular: '',
                            asunto: '',
                            mensaje: ''
                        };
                    }).catch(error => {
                    this.enviado = false;
                    this.errors = error.response.data;
                    this.loading = false;
                });
            },
        }
    });
}