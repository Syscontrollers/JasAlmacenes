<?php
	$row_url = "http://jasalmacenes.com";
	$conf_web_titulo = "JAS Almacenes";
	$row_titulo = "JAS Almacenes - Líder en soluciones de almacenamiento";
	$row_descripcion = "Líder en soluciones de almacenamiento. Brindamos nuestras soluciones para importadores y exportadores en la ciudad de Ilo.";
	$row_keywords = "almacenamiento, importadores, exportadores, importacion, exportacion, soluciones, ilo, peru, almacenaje, proteccion, seguridad";
	$row_imagen_original = "/imagenes/fondo-logo.png";
?>
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>JAS ALMACENES S.A</title>

	<meta name="description" content="">

	<meta name="robots" content="index,follow"/>
	<meta name='googlebot' content='index,follow'/>
	<meta property="fb:app_id" content="255520401560154">
	<meta name="token" id="token" content="{{ csrf_token() }}">
	<link rel="shortcut icon" href="{{ asset('favicon.png') }}">
	<link rel="icon" sizes="96x96" href="{{ asset('favicon.png') }}">

	<link rel="canonical" href="{{ $row_url }}" />

	<meta name="description" content="{{ $row_descripcion }}">
	<meta name="keywords" content="{{ $row_keywords }}">

	<meta property="og:type" content="article" />
	<meta property="og:title" content="{{ $row_titulo }}" />
	<meta property="og:description" content="{{ $row_descripcion }}" />
	<meta property="og:url" content="{{ $row_url }}" />
	<meta property="og:image" content="{{ asset($row_imagen_original) }}" />

	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:title" content="{{ $row_titulo }}">
	<meta name="twitter:description" content="{{ $row_descripcion }}">
	<meta name="twitter:image" content="{{ asset($row_imagen_original) }}">

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@id": "{{ $row_url }}",
			"@type": "Organization",
			"name": "{{ $conf_web_titulo }}",
			"url": "{{ $row_url }}",
			"logo": "{{ asset("image/logojas.svg") }}",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "Av. Santo Toribio 143 Departamento 601 San isidros"
			},
			"contactPoint": [
				{
					"@type": "ContactPoint",
					"telephone": "(+511) 715-5192",
					"contactType": "customer support",
					"areaServed": [ "PE" ]
				}
			]
		}
	</script>

	{{-- Enlaces Externos --}}
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Oswald:300,400,500,700|Raleway:300,400,500,700,900">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/css/lightbox.min.css">
	<link href="https://file.myfontastic.com/pCZqJSNeuTMJc6e6CDCeyG/icons.css" rel="stylesheet">

	{{-- Enlaces Internos --}}
	<link rel="stylesheet" href="css/styles.css">
	<link rel="stylesheet" href="css/media.css">
	<link rel="stylesheet" href="css/general.min.css">
	<link rel="stylesheet" href="css/animate.min.css">
	<link rel="stylesheet" href="css/icons.css">
</head>
<body>
<section class="container container_banner" id="inicio">
		<div class="navigation navigation_contenedor">
			<div class="container_banner__imagotipo">
				<a href="#inicio"><img src="image/logojas.svg" alt=""></a>
			</div>
			<div class="container_banner__icon menu_bar">
				<span class="icon-nav"></span>
			</div>
			<nav class="nav nav-desktop">
					<ul>
						<li><a href="#inicio">inicio</a></li>
						<li><a href="#nosotros">nosotros</a></li>
						<li><a href="#servicios">servicios</a></li>
						<li><a href="#galeria">galeria</a></li>
						<li><a href="#contacto">contacto</a></li>
					</ul>
			</nav>
		</div>
		<nav class="nav nav-movil">
				<ul>
					<li><a href="#inicio">inicio</a></li>
					<li><a href="#nosotros">nosotros</a></li>
					<li><a href="#servicios">servicios</a></li>
					<li><a href="#galeria">galeria</a></li>
					<li><a href="#contacto">contacto</a></li>
				</ul>
		</nav>
		<div class="container_banner__svg">
			<img src="image/bg-banner2.svg" alt="">
		</div>
		<div class="container_copy banner_copy wow bounceIn">
			<h1>líder en soluciones de almacenamiento</h1>
			<p>Brindamos nuestras soluciones para importadores y exportadores en la ciudad de Ilo.</p>
			<a class="formlulario" href="#contacto">contactanos</a>
		</div>
	</section>
	<!------------>
	<!------------>
	<section class="container container_nosotros" id="nosotros">
		<div class="container_copy nosotros_copy wow bounceIn">
			<h2>Acerca de <span> nosotros</span></h2>
			<p>Ofrecemos soluciones de almacenamiento para actividades de comercio exterior, nuestra gran capacidad de infraestructura y tecnología nos permite garantizar un servicio confiable y eficiente.</p>
		</div>
		<div class="container_nosotros_items wow bounceInUp">
			<div class="container_nosotros_item">
				<span>Misión</span>
				<p class="info">Ser la empresa líder en brindar un excelente servicio en soluciones de almacenamiento para Importadores y Exportadores en la ciudad de ILO, satisfaciendo las necesidades de nuestros clientes.</p>
			</div>
			<div class="container_nosotros_item">
				<span>Visión</span>
				<p class="info">Ofrecer el mejor servicio en depósitos y terminales de Almacenamiento Aduanero.</p>
			</div>
		</div>
		<div class="container_nosotros__svg"></div>
	</section>
	<!------------>
	<!------------>
	<section class="container container_servicios" id="servicios">
		<div class="container_copy servicios_copy wow bounceIn">
			<h2>Nuestros <span> servicios</span></h2>
			<p>Desde el año 1996, venimos operando en este rubro. Las instalaciones de nuestra propiedad se encuentran en la ciudad de ILO, a 6 kilómetros del Terminal Portuario de ILO y a 200 metros del aeropuerto, contando con un Área de 120,000m2, totalmente protegida con cerco perimetral en toda su extensión de material noble y con torreones de vigilancia, parcialmente techada en las áreas para el almacenamiento de mercaderías que así lo requieran.</p>
			<p>Contamos con todas las facilidades propias del Giro del negocio, tales como recepción y almacenaje de mercadería de importación y exportación, como son: maquinarias, vehículos, motores, textiles y carga en general. Balanza Electrónica de 80 Ton. de capacidad, disposición de contenedores, oficinas para personal propio y para el personal de la Intendencia de Aduanas de Ilo, para dar apoyo a personal de Agencias de Aduana, importadores, etc.; nuestras instalaciones cuentan con  servicios higiénicos y sistema de protección contra incendio abastecidos con tubería independiente desde la Planta de tratamiento local, líneas telefónicas, servicio de cámaras de vigilancia estratégicamente instaladas, internet, asimismo un área de 13,741m2 destinada como Depósito Temporal (Zona Primaria) con código de aduana N° 4239,en la circunscripción de Intendencia de Aduana de ILO.</p>
			<p>También ofrecemos el servicio de alquiler de áreas libres por m², así como el servicio de elevador de 13 toneladas y servicio de Staker.</p>
		</div>
		<div class="container_servicios__svg">
			<img class="servicios-box" src="image/bg-servicios6.svg" alt="">
			<img class="servicios-item" src="image/boxmov.svg" alt="">
			<img src="image/bg-servicios5.svg" alt="">
		</div>
	</section>
	<!------------>
	<!------------>
	<section class="container container_servicios container_galeria" id="galeria">
		<div class="container_copy servicios_copy wow bounceIn">
			<h2>Nuestras <span> instalaciones</span></h2>
			<p>Nos ubicamos en la ciudad de ILO, a 6 kilómetros del terminal portuario de Ilo y a 200 metros del aeropuerto, contando con un área de 120,000m2, totalmente protegida con cerco perimetral en toda su extensión de material noble y con torreones de vigilancia, parcialmente techada en las áreas para el almacenamiento de mercaderías que así lo requieran.</p>
		</div>
		<div class="servicios_images wow bounceInUp">
			<div class="servicios_images__items instalaciones_images__items">
				<div class="items__servicios">
					<img src="image/bg-instalaciones2.svg" alt="">
				</div>
				<span>Oficinas</span>
				<p class="info_instalaciones">Oficinas para el personal propio y para el personal de la Intendencia de Aduanas de Ilo.</p>
			</div>
			<div class="servicios_images__items instalaciones_images__items">
				<div class="items__servicios">
					<img src="image/bg-instalaciones.svg" alt="">
				</div>
				<span>Seguridad</span>
				<p class="info_instalaciones">Servicio de cámaras de vigilancia estratégicamente instaladas.</p>
			</div>
			<div class="servicios_images__items instalaciones_images__items">
				<div class="items__servicios">
					<img src="image/bg-instalaciones3.svg" alt="">
				</div>
				<span>Protección</span>
				<p class="info_instalaciones">Sistema de protección contra incendio abastecidos con tubería independiente.</p>
			</div>
			<div class="servicios_images__items instalaciones_images__items">
				<div class="items__servicios">
					<img src="image/bg-instalaciones4.svg" alt="">
				</div>
				<span>Plugin</span>
				<p class="info_instalaciones">Contamos con una balanza electrónica de 80 toneladas de capacidad.</p>
			</div>
		</div>
		<div class="galeria_images wow bounceInUp">
			<ul class="galeria">
				<li class="galeria__item">
					<a href="image/1-min.jpg" data-lightbox="galeria">
						<img src="image/1-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/2-min.jpg" data-lightbox="galeria">
						<img src="image/2-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/3-min.jpg" data-lightbox="galeria">
						<img src="image/3-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/4-min.jpg" data-lightbox="galeria">
						<img src="image/4-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/5-min.jpg" data-lightbox="galeria">
						<img src="image/5-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/6-min.jpg" data-lightbox="galeria">
						<img src="image/6-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/7-min.jpg" data-lightbox="galeria">
						<img src="image/7-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/8-min.jpg" data-lightbox="galeria">
						<img src="image/8-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/9-min.jpg" data-lightbox="galeria">
						<img src="image/9-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/10-min.jpg" data-lightbox="galeria">
						<img src="image/10-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/11-min.jpg" data-lightbox="galeria">
						<img src="image/11-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/12-min.jpg" data-lightbox="galeria">
						<img src="image/12-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/13-min.jpg" data-lightbox="galeria">
						<img src="image/13-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/14-min.jpg" data-lightbox="galeria">
						<img src="image/14-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/15-min.jpg" data-lightbox="galeria">
						<img src="image/15-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/16-min.jpg" data-lightbox="galeria">
						<img src="image/16-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/17-min.jpg" data-lightbox="galeria">
						<img src="image/17-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
				<li class="galeria__item">
					<a href="image/18-min.jpg" data-lightbox="galeria">
						<img src="image/18-min.jpg" class="galeria__img" alt="">
					</a>
				</li>
			</ul>
		</div>
	</section>
<!------------>
<!------------>
<section class="container container_clientes" id="clientes">
		<div class="container_copy clientes_copy wow bounceIn">
			<h2>Nuestros <span> clientes</span></h2>
			<p>Ya son muchas las empresas que se han beneficiado con nuestros servicios brindados.</p>
		</div>
		<div class="clientes_images wow bounceInUp">
			<div class="clientes_images__items">
				<img src="image/1.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/2.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/3.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/4.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/5.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/6.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/7.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/8.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/9.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/10.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/11.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/12.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/13.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/logistic-travel-ilo.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/marko.png" alt="">
			</div>
			<div class="clientes_images__items">
				<img src="image/facilidad-portuaria.png" alt="">
			</div>
		</div>
		<div class="container_clientes__image wow">
			<div class="container_clientes__svg"></div>
		</div>
	</section>
<!------------>
<!------------>
<section id="formulario_contacto" class="container container_contacto">
	<div class="container_contacto__form" v-if="!enviado">
		<form id="contacto">
			<div class="form_imagotipo wow bounceIn">
				<h2>Pongámonos en <span> contacto </span></h2>
			</div>
			<input type="text" placeholder="Nombre y Apellido" :class="{'input-error': errors.nombres}" v-model="datos.nombres">
			<input type="email" placeholder="Correo Electrónico" :class="{'input-error': errors.email}" v-model="datos.email">
			<input type="text" placeholder="Empresa" :class="{'input-error': errors.empresa}" v-model="datos.empresa">
			<input type="text" placeholder="Teléfono" :class="{'input-error': errors.telefono}" v-model="datos.telefono">
			<textarea name="" placeholder="Dejenos un mensaje" :class="{'input-error': errors.mensaje}" v-model="datos.mensaje"></textarea>
			<div v-show="loading" class="loader"></div>
			<a href="#" @click.prevent="guardar">Contáctanos</a>
		</form>
	</div>
	<div class="container_contacto__form" v-else>
		<form id="contacto">
			<div class="form_imagotipo wow bounceIn">
				<h2>El mensaje se envío con éxito.</h2>
			</div>
		</form>
	</div>
	<div class="mapa_ubicanos">
		<div class="mapa_ubicaciones__contenedor">
				<div class="ubicaciones_contenedor">
						<h4>Ubicanos</h4>
						<ul>
							<li><span class="icon icon-home"></span>Dirección en Lima: Av. Santo Toribio 143 Departamento 601 San isidro</li>
							<li><span class="icon icon-home"></span>Dirección en ILO: Carretera Costanera Sur Km 7 S/N (Frente al Aeropuerto) - ILO – Moquegua</li>
							<li><span class="icon icon-telefono"></span>Teléfonos: 715-5192 </li>
							<li><span class="icon icon-wp"></span>Celular: 946080220 / 960273452</li>
						</ul>
				</div>
		</div>
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6762.340962566257!2d-70.89405646651095!3d-17.967776361924656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9144fe09e2bf7379%3A0x7b6a615cf3c4f0fc!2sCostanera+Sur!5e0!3m2!1ses-419!2spe!4v1521158852311" frameborder="0" style="border:0" allowfullscreen></iframe>

	</div>
</section>
<!------------>
<!------------>
<footer class="footer">
		<div class="footer_contenedor">
			<p>Copyright © 2018 - JAS Almacenes - Todos los derechos reservados.</p>
			<div class="">
					<p>Desarrollado por </p><a href="http://chilcanoweb.com/" target="_blank"> Chilcano Web</a>
			</div>
		</div>
	</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.18.0/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.10.0/js/lightbox.min.js"></script>
<script src="/js/app.js"></script>
<script src="/js/wow.js"></script>
<script>
    new WOW().init();
</script>
<script src="/js/scroll.js"></script>
<script src="/js/sticky.min.js"></script>
<script src="/js/nav.js"></script>
</body>
</html>