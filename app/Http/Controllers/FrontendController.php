<?php

namespace App\Http\Controllers;

use App\Mail\ContactoAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FrontendController extends Controller
{
    public function index()
    {
        return view('frontend.index');
    }

    public function contacto(Request $request)
    {
        $rules = [
            'nombres' => 'required',
            'email' => 'required|email',
            'empresa' => 'required',
            'telefono' => 'required',
            'mensaje' => 'required'
        ];

        $this->validate($request, $rules);

        Mail::send('emails.contacto-admin', ['request' => $request], function ($m) use ($request) {
            $m->from('no-reply@jasalmacenes.com');
            $m->to('ojaila@jasalmacenes.com');
            $m->bcc('tnastasi@jasalmacenes.com');
            $m->subject($request->nombres . 'te ha enviado un mensaje - JAS Almacenes');
        });

        return "Mensaje enviado";
    }
}
